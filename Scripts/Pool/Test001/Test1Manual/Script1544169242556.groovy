import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://katalon-demo-cura.herokuapp.com/')

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Pool/Test1/Page_CURA Healthcare Service/h1_CURA Healthcare Service'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Pool/Test1/Page_CURA Healthcare Service/a_Make Appointment'))

WebUI.verifyElementVisible(findTestObject('Pool/Test1/Page_CURA Healthcare Service/h2_Login'))

WebUI.delay(1)

WebUI.setText(findTestObject('Pool/Test1/Page_CURA Healthcare Service/input_Username_username'), name)

WebUI.setEncryptedText(findTestObject('Pool/Test1/Page_CURA Healthcare Service/input_Password_password'), pass)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Pool/Test1/Page_CURA Healthcare Service/button_Login'))

WebUI.verifyElementClickable(findTestObject('Pool/Test1/Page_CURA Healthcare Service/button_Book Appointment'))

WebUI.delay(1)

WebUI.click(findTestObject('Pool/Test1/Page_CURA Healthcare Service/input_Apply for hospital readm'))

WebUI.click(findTestObject('Pool/Test1/Page_CURA Healthcare Service/input_None_programs'))

WebUI.setText(findTestObject('Pool/Test1/Page_CURA Healthcare Service/input_Visit Date (Required)_vi'), date)

WebUI.setText(findTestObject('Pool/Test1/Page_CURA Healthcare Service/textarea_Comment_comment'), text)

WebUI.click(findTestObject('Pool/Test1/Page_CURA Healthcare Service/textarea_Comment_comment'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Pool/Test1/Page_CURA Healthcare Service/button_Book Appointment'))

WebUI.verifyElementClickable(findTestObject('Pool/Test1/Page_CURA Healthcare Service/a_Go to Homepage'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Pool/Test1/Page_CURA Healthcare Service/a_CURA Healthcare_menu-toggle'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Pool/Test1/Page_CURA Healthcare Service/a_Logout'))

WebUI.verifyElementClickable(findTestObject('Pool/Test1/Page_CURA Healthcare Service/a_Make Appointment'))

WebUI.takeScreenshot()

WebUI.closeBrowser()

