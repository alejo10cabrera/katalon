
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */


def static "cursoDiciembre.TestDataGenerator.getRandomParagraphs"(
    	int a	
     , 	int b	) {
    (new cursoDiciembre.TestDataGenerator()).getRandomParagraphs(
        	a
         , 	b)
}

def static "cursoDiciembre.TestDataGenerator.getRandomName"(
    	int a	
     , 	int b	) {
    (new cursoDiciembre.TestDataGenerator()).getRandomName(
        	a
         , 	b)
}

def static "cursoDiciembre.TestDataGenerator.getRandomFemaleName"() {
    (new cursoDiciembre.TestDataGenerator()).getRandomFemaleName()
}

def static "cursoDiciembre.TestDataGenerator.getRandomMaleName"() {
    (new cursoDiciembre.TestDataGenerator()).getRandomMaleName()
}

def static "cursoDiciembre.TestDataGenerator.getRandomFirstName"() {
    (new cursoDiciembre.TestDataGenerator()).getRandomFirstName()
}

def static "cursoDiciembre.TestDataGenerator.getRandomLastName"() {
    (new cursoDiciembre.TestDataGenerator()).getRandomLastName()
}

def static "cursoDiciembre.TestDataGenerator.getRandomCity"() {
    (new cursoDiciembre.TestDataGenerator()).getRandomCity()
}

def static "cursoDiciembre.TestDataGenerator.getRandomHeading"(
    	int numberofWords	) {
    (new cursoDiciembre.TestDataGenerator()).getRandomHeading(
        	numberofWords)
}

def static "cursoDiciembre.TestDataGenerator.getRandomHeading_two"(
    	int Wordsfrom	
     , 	int wordstwo	) {
    (new cursoDiciembre.TestDataGenerator()).getRandomHeading_two(
        	Wordsfrom
         , 	wordstwo)
}

def static "cursoDiciembre.TestDataGenerator.getRandomEmail"() {
    (new cursoDiciembre.TestDataGenerator()).getRandomEmail()
}

def static "com.UAT.util.ScreenshotTime.takeScreenshotDate"() {
    (new com.UAT.util.ScreenshotTime()).takeScreenshotDate()
}
