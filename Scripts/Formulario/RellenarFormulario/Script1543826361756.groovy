import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://katalon-test.s3.amazonaws.com/demo-aut/dist/html/form.html')

WebUI.setText(findTestObject('Object Repository/Page_Demo AUT/input_First name_firstName'), 'Alejo')

WebUI.setText(findTestObject('Object Repository/Page_Demo AUT/input_Last name_lastName'), 'Cabrera')

WebUI.click(findTestObject('Object Repository/Page_Demo AUT/input_Male_gender'))

WebUI.setText(findTestObject('Object Repository/Page_Demo AUT/input_Date of birth_dob'), '01/03/1994')

WebUI.setText(findTestObject('Object Repository/Page_Demo AUT/input_Address_address'), 'Calle Falsa Nº 123')

WebUI.setText(findTestObject('Object Repository/Page_Demo AUT/input_Email_email'), 'alcabrer@everis.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Demo AUT/input_Password_password'), 'cfxHDgFvIy+ZL3/6JGyo3Q==')

WebUI.setText(findTestObject('Object Repository/Page_Demo AUT/input_Company_company'), 'Everis')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Demo AUT/select_High salaryNice manager'), 'High salary', 
    true)

WebUI.click(findTestObject('Object Repository/Page_Demo AUT/input'))

WebUI.click(findTestObject('Object Repository/Page_Demo AUT/input'))

WebUI.click(findTestObject('Object Repository/Page_Demo AUT/input'))

WebUI.setText(findTestObject('Object Repository/Page_Demo AUT/textarea_Comment_comment'), 'Estoy haciendo un curso de Katalon')

WebUI.click(findTestObject('Object Repository/Page_Demo AUT/button_Submit'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Demo AUT/span_Successfully submitted'))

WebUI.closeBrowser()

