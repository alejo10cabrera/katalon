import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://katalon-test.s3.amazonaws.com/demo-aut/dist/html/form.html')

WebUI.click(findTestObject('Object Repository/Page_Demo AUT/button_Submit (1)'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Demo AUT/label_This field is required.'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Demo AUT/label_This field is required._1'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Demo AUT/label_This field is required._2'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Demo AUT/label_This field is required._3'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Demo AUT/label_This field is required._4'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Demo AUT/label_This field is required._5'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Demo AUT/label_This field is required._6'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Demo AUT/label_This field is required._7'))

WebUI.closeBrowser()

