<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>suitecsv</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a5ecfcd5-e4dd-4290-8097-f38df3e4f1ad</testSuiteGuid>
   <testCaseLink>
      <guid>ba444716-e158-492d-81e1-17c8f3854072</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CSV/NewCSV</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d09dcf85-602a-4fce-8950-346298f519b5</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>d09dcf85-602a-4fce-8950-346298f519b5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>First name</value>
         <variableId>da55fe95-893b-49e4-bb38-b7d7d19d2b33</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d09dcf85-602a-4fce-8950-346298f519b5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Last name</value>
         <variableId>5a37b22e-952f-4044-8820-25dad6ec62f4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d09dcf85-602a-4fce-8950-346298f519b5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>c625b403-fa58-421a-b920-25ce815c281d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d09dcf85-602a-4fce-8950-346298f519b5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>cd025d4e-2feb-4f3b-8853-137b017c7e9b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
