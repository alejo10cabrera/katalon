import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://katalon-test.s3.amazonaws.com/demo-aut/dist/html/form.html')

WebUI.maximizeWindow()

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input_First name_firstName'), 'Alejo')

WebUI.setText(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input_Last name_lastName'), 'Cabrera')

WebUI.click(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input_Male_gender'))

WebUI.setText(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input_Date of birth_dob'), '01/03/1994')

WebUI.setText(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input_Address_address'), 'Calle Médico José Ferreira Quintana 15')

WebUI.setText(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input_Email_email'), 'alcabrer@everis.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input_Password_password'), 'cfxHDgFvIy+ZL3/6JGyo3Q==')

WebUI.setText(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input_Company_company'), 'Everis')

WebUI.selectOptionByValue(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/select_DeveloperQAManagerTechn'), 'QA', 
    true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/select_High salaryNice manager'), 'Good teamwork', 
    true)

WebUI.click(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input'))

WebUI.click(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/input'))

WebUI.setText(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/textarea_Comment_comment'), 'Esto es otra prueba automatizada del curso de Katalon de Rafa')

WebUI.click(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/button_Submit'))

WebUI.delay(2)

WebUI.verifyElementClickable(findTestObject('Object Repository/Pool/Test2/Page_Demo AUT/span_Successfully submitted'))

WebUI.closeBrowser()

