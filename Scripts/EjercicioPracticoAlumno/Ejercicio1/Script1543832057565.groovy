import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://cts.everis.int/launcher/login')

WebUI.setText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus'), 
    'alcabrer')

WebUI.setEncryptedText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus _1'), 
    'XVhcGZ1UI+8=')

WebUI.setEncryptedText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus _2'), 
    'Sv51rRG7eNw=')

WebUI.setEncryptedText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus _3'), 
    'sqU3kcU42wg=')

WebUI.setEncryptedText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus _4'), 
    'okbcS/6irwc=')

WebUI.setEncryptedText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus _5'), 
    '4OsemxNEfac=')

WebUI.setEncryptedText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus _6'), 
    'BrGlvxf2TvE=')

WebUI.setEncryptedText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus _7'), 
    'UQQgsJs35IQ=')

WebUI.setEncryptedText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus _8'), 
    'cfxHDgFvIy+ZL3/6JGyo3Q==')

WebUI.setEncryptedText(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/input_Por favor introduce tus _9'), 
    'cfxHDgFvIy+ZL3/6JGyo3Q==')

WebUI.click(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/button_Acceder'))

WebUI.click(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_CTS App Launcher/span_Development_icon-basic-we'))

WebUI.switchToWindowTitle('katalon wikipedia - Buscar con Google')

WebUI.click(findTestObject('Object Repository/CasoPractico/Page_CTS App Launcher/Page_katalon wikipedia - Buscar con/h3_Katalon Studio - Wikipedia'))

