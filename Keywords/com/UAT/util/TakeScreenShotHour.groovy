package com.UAT.util

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import com.kms.katalon.core.annotation.Keyword
import java.text.SimpleDateFormat


public class ScreenshotTime {
	
	@Keyword
	public void takeScreenshotDate(){
		
		try {
			Date data = new Date(System.currentTimeMillis())
			
			SimpleDateFormat formatarDate = new SimpleDateFormat('yyyyMMddHHmmss')
			
			WebUI.takeScreenshot(('C:\\screenshot\\_' + formatarDate.format(data)) + '.png')
				}
				catch (Exception e) {
					e.printStackTrace()
				}
		
	}
}