import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://demo.nopcommerce.com/')

WebUI.click(findTestObject('Object Repository/CSV/Page_nopCommerce demo store/a_Register'))

WebUI.setText(findTestObject('Object Repository/CSV/Page_nopCommerce demo store. Regist/input_First name_FirstName'), name)

WebUI.setText(findTestObject('Object Repository/CSV/Page_nopCommerce demo store. Regist/input_Last name_LastName'), surname)

WebUI.setText(findTestObject('Object Repository/CSV/Page_nopCommerce demo store. Regist/input_Email_Email'), email)

WebUI.setEncryptedText(findTestObject('Object Repository/CSV/Page_nopCommerce demo store. Regist/input_Password_Password'), 
    pass)

WebUI.setEncryptedText(findTestObject('Object Repository/CSV/Page_nopCommerce demo store. Regist/input_Confirm password_Confirm'), 
    pass)

WebUI.click(findTestObject('Object Repository/CSV/Page_nopCommerce demo store. Regist/input__register-button'))

