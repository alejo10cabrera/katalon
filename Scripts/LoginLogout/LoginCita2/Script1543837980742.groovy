import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

CustomKeywords.'com.UAT.util.ScreenshotTime.takeScreenshotDate'()

WebUI.navigateToUrl('https://katalon-demo-cura.herokuapp.com/')

CustomKeywords.'com.UAT.util.ScreenshotTime.takeScreenshotDate'()

WebUI.click(findTestObject('Object Repository/LoginLogout/Page_CURA Healthcare Service/a_Make Appointment'))

CustomKeywords.'com.UAT.util.ScreenshotTime.takeScreenshotDate'()

WebUI.setText(findTestObject('Object Repository/LoginLogout/Page_CURA Healthcare Service/input_Username_username'), user)

CustomKeywords.'com.UAT.util.ScreenshotTime.takeScreenshotDate'()

WebUI.setEncryptedText(findTestObject('Object Repository/LoginLogout/Page_CURA Healthcare Service/input_Password_password'), 
    pass)

CustomKeywords.'com.UAT.util.ScreenshotTime.takeScreenshotDate'()

WebUI.click(findTestObject('Object Repository/LoginLogout/Page_CURA Healthcare Service/button_Login'))

CustomKeywords.'com.UAT.util.ScreenshotTime.takeScreenshotDate'()

WebUI.click(findTestObject('Page_CURA Healthcare Service/select_Tokyo CURA Healthcare C'))

CustomKeywords.'com.UAT.util.ScreenshotTime.takeScreenshotDate'()

WebUI.setText(findTestObject('Page_CURA Healthcare Service/input_Visit Date (Required)_vi'), '12/23/2018')

CustomKeywords.'com.UAT.util.ScreenshotTime.takeScreenshotDate'()

WebUI.setText(findTestObject('Page_CURA Healthcare Service/textarea_Comment_comment'), 'Fiebre y alucinaciones')

CustomKeywords.'com.UAT.util.ScreenshotTime.takeScreenshotDate'()

WebUI.click(findTestObject('Page_CURA Healthcare Service/button_Book Appointment'))

